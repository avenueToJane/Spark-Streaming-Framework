package bigdata.java.platform.beans;

import java.util.Date;

public class TaskLog {

    /** taskLogId */
    private Integer taskLogId;

    /** taskId */
    private Integer taskId;

    /** livy的id */
    private Integer batchId;

    /** yarn的applicationId */
    private String applicationid;

    /** livy的session */
    private String session;

    /** driver的id */
    private String driver;

    /** sparkUI的地址 */
    private String sparkui;

    /** 更新日期 */
    private Date updateTime;

    /** livy返回的json字符串 */
    private String livyText;

    /** 积压批次 */
    private Integer waitingBatches;

    /** 完成总数 */
    private Integer totalCompletedBatches;

    /** 处理总记录数 */
    private Long totalProcessedRecords;

    /** sparkUI返回的json统计数据 */
    private String metrics;

    /*  */

    /*  */

    /**
     * 获取taskLogId
     *
     * @return taskLogId
     */
    public Integer getTaskLogId() {
        return this.taskLogId;
    }

    /**
     * 设置taskLogId
     *
     * @param taskLogId
     */
    public void setTaskLogId(Integer taskLogId) {
        this.taskLogId = taskLogId;
    }

    /**
     * 获取taskId
     *
     * @return taskId
     */
    public Integer getTaskId() {
        return this.taskId;
    }

    /**
     * 设置taskId
     *
     * @param taskId
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    /**
     * 获取livy的id
     *
     * @return livy的id
     */
    public Integer getBatchId() {
        return this.batchId;
    }

    /**
     * 设置livy的id
     *
     * @param batchId
     *          livy的id
     */
    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    /**
     * 获取yarn的applicationId
     *
     * @return yarn的applicationId
     */
    public String getApplicationid() {
        return this.applicationid;
    }

    /**
     * 设置yarn的applicationId
     *
     * @param applicationid
     *          yarn的applicationId
     */
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid;
    }

    /**
     * 获取livy的session
     *
     * @return livy的session
     */
    public String getSession() {
        return this.session;
    }

    /**
     * 设置livy的session
     *
     * @param session
     *          livy的session
     */
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * 获取driver的id
     *
     * @return driver的id
     */
    public String getDriver() {
        return this.driver;
    }

    /**
     * 设置driver的id
     *
     * @param driver
     *          driver的id
     */
    public void setDriver(String driver) {
        this.driver = driver;
    }

    /**
     * 获取sparkUI的地址
     *
     * @return sparkUI的地址
     */
    public String getSparkui() {
        return this.sparkui;
    }

    /**
     * 设置sparkUI的地址
     *
     * @param sparkui
     *          sparkUI的地址
     */
    public void setSparkui(String sparkui) {
        this.sparkui = sparkui;
    }

    /**
     * 获取更新日期
     *
     * @return 更新日期
     */
    public Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * 设置更新日期
     *
     * @param updateTime
     *          更新日期
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取livy返回的json字符串
     *
     * @return livy返回的json字符串
     */
    public String getLivyText() {
        return this.livyText;
    }

    /**
     * 设置livy返回的json字符串
     *
     * @param livyText
     *          livy返回的json字符串
     */
    public void setLivyText(String livyText) {
        this.livyText = livyText;
    }

    /**
     * 获取积压批次
     *
     * @return 积压批次
     */
    public Integer getWaitingBatches() {
        return this.waitingBatches;
    }

    /**
     * 设置积压批次
     *
     * @param waitingBatches
     *          积压批次
     */
    public void setWaitingBatches(Integer waitingBatches) {
        this.waitingBatches = waitingBatches;
    }

    /**
     * 获取完成总数
     *
     * @return 完成总数
     */
    public Integer getTotalCompletedBatches() {
        return this.totalCompletedBatches;
    }

    /**
     * 设置完成总数
     *
     * @param totalCompletedBatches
     *          完成总数
     */
    public void setTotalCompletedBatches(Integer totalCompletedBatches) {
        this.totalCompletedBatches = totalCompletedBatches;
    }

    /**
     * 获取处理总记录数
     *
     * @return 处理总记录数
     */
    public Long getTotalProcessedRecords() {
        return this.totalProcessedRecords;
    }

    /**
     * 设置处理总记录数
     *
     * @param totalProcessedRecords
     *          处理总记录数
     */
    public void setTotalProcessedRecords(Long totalProcessedRecords) {
        this.totalProcessedRecords = totalProcessedRecords;
    }

    /**
     * 获取sparkUI返回的json统计数据
     *
     * @return sparkUI返回的json统计数据
     */
    public String getMetrics() {
        return this.metrics;
    }

    /**
     * 设置sparkUI返回的json统计数据
     *
     * @param metrics
     *          sparkUI返回的json统计数据
     */
    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }
}

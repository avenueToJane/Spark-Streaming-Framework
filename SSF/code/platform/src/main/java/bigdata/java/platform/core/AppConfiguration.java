package bigdata.java.platform.core;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * 设置config类 使用 @Bean注入 fastJsonHttpMessageConvert（替换springboot 自带的jackson为fastjson）
 */
@Configuration
public class AppConfiguration {
    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        // 1、需要先定义一个 convert 转换消息的对象;
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        // 2、添加fastJson 的配置信息，比如：是否要格式化返回的json数据;
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(
                SerializerFeature.PrettyFormat
                ,SerializerFeature.WriteNullStringAsEmpty  //null或者空字符串也需要生产json的key
                ,SerializerFeature.DisableCircularReferenceDetect  //禁止通过循环引用生产json
        );
        // 3、处理中文乱码问题aaa
        List<MediaType> fastMediaTypes = new ArrayList<MediaType>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConverter.setSupportedMediaTypes(fastMediaTypes);
        // 4、在convert中添加配置信息.
        fastConverter.setFastJsonConfig(fastJsonConfig);
        HttpMessageConverter<?> converter = fastConverter;
        return new HttpMessageConverters(converter);
    }

    @Autowired
    DataSource dataSource;
    @Bean(name="namedParameterJdbcTemplate")
    public NamedJdbcTemplate namedParameterJdbcTemplate()
    {
        NamedJdbcTemplate sampleNamedParameterJdbcTemplate = new NamedJdbcTemplate(dataSource);
        return sampleNamedParameterJdbcTemplate;
    }

    @Bean(name="jdbcTemplate")
    public MyJdbcTemplate jdbcTemplate()
    {
        MyJdbcTemplate jdbcTemplate = new MyJdbcTemplate(dataSource);
        return jdbcTemplate;
    }

}

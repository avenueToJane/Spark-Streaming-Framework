/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.kafka;

import bigdata.java.framework.spark.pool.ConnectionFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.kafka.clients.producer.KafkaProducer;
import java.util.Properties;

public class KafkaProducerConnectionFactory implements ConnectionFactory<KafkaProducer<String,String>> {

    KafkaProducerConfig kafkaProducerConfig;

    public KafkaProducerConnectionFactory(KafkaProducerConfig kafkaProducerConfig)
    {
        this.kafkaProducerConfig = kafkaProducerConfig;
    }

    @Override
    public KafkaProducer<String, String> createConnection() throws Exception {
        Properties properties = kafkaProducerConfig.getProperties();
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);
        return producer;
    }

    @Override
    public PooledObject<KafkaProducer<String, String>> makeObject() throws Exception {
        KafkaProducer<String, String> producer = this.createConnection();
        return new DefaultPooledObject<>(producer);
    }

    @Override
    public void destroyObject(PooledObject<KafkaProducer<String, String>> p) throws Exception {
        KafkaProducer<String, String> producer = p.getObject();
        if(producer!= null)
        {
            producer.close();
        }
    }

    @Override
    public boolean validateObject(PooledObject<KafkaProducer<String, String>> p) {
        KafkaProducer<String, String> producer = p.getObject();
        return producer !=null;
    }

    @Override
    public void activateObject(PooledObject<KafkaProducer<String, String>> p) throws Exception {

    }

    @Override
    public void passivateObject(PooledObject<KafkaProducer<String, String>> p) throws Exception {

    }
}

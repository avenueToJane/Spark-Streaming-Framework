/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.InputParameterUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import java.util.Map;

public class SKContext
{
    InputParameterUtil inputParameterUtil;
    SparkConf sparkConf;
    JavaStreamingContext jsc;
    KafkaOffsetPersist kafkaOffsetPersist;
    Map<String, Object> kafkaParams;

    public JavaInputDStream<ConsumerRecord<String, String>> getDirectStream() {
        return directStream;
    }

    public void setDirectStream(JavaInputDStream<ConsumerRecord<String, String>> directStream) {
        this.directStream = directStream;
    }

    JavaInputDStream<ConsumerRecord<String, String>> directStream;

    public InputParameterUtil getInputParameterUtil() {
        return inputParameterUtil;
    }

    public void setInputParameterUtil(InputParameterUtil inputParameterUtil) {
        this.inputParameterUtil = inputParameterUtil;
    }

    public SparkConf getSparkConf() {
        return sparkConf;
    }

    public void setSparkConf(SparkConf sparkConf) {
        this.sparkConf = sparkConf;
    }

    public JavaStreamingContext getJsc() {
        return jsc;
    }

    public void setJsc(JavaStreamingContext jsc) {
        this.jsc = jsc;
    }

    public KafkaOffsetPersist getKafkaOffsetPersist() {
        return kafkaOffsetPersist;
    }

    public void setKafkaOffsetPersist(KafkaOffsetPersist kafkaOffsetPersist) {
        this.kafkaOffsetPersist = kafkaOffsetPersist;
    }

    public Map<String, Object> getKafkaParams() {
        return kafkaParams;
    }

    public void setKafkaParams(Map<String, Object> kafkaParams) {
        this.kafkaParams = kafkaParams;
    }
}
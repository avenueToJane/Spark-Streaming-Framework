/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import org.apache.spark.streaming.kafka010.OffsetRange;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class KafkaOffsetPersist implements Serializable {

    String appName;
    String topic;
    String groupId;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
    public KafkaOffsetPersist(String appName, String topic, String groupId){
        this.appName = appName;
        this.topic = topic;
        this.groupId = groupId;
    }

    public Map<String,Object> getConstructorParams(){
        Map<String,Object> map = new HashMap<>();
        map.put("appName",appName);
        map.put("topic",topic);
        map.put("groupId",groupId);
        return map;
    }

    public abstract void clearPersist();

    public abstract boolean persistOffset(OffsetRange[] offsetRanges);

    public abstract List<TopicGroupPartitionOffset> getOffset();
}

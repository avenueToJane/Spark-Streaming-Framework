package bigdata.java.spark.streaming.task.datasynchro;

import bigdata.java.framework.spark.kafka.KafkaOffsetPersist;
import bigdata.java.framework.spark.kafka.SKProcessor;
import bigdata.java.framework.spark.kafka.SimpleSparkKafka;
import bigdata.java.framework.spark.util.JsonUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author
 */
public class DataSynchroStreaming {
    public static void main(String[] args) throws InterruptedException {
        //在idea中调试时，Program arguments 参数
        //appname:FindDataStreaming topics:yxgk.a_Cashchk_Flow duration:2 maxnum:2000
        //appname=应用程序名称
        //topics=消费的kafka主题，多个主题用逗号隔开
        //duration=每隔多少秒执行一个spark批次
        //maxnum=每个批次获取多少数据，每隔批次消费的总数据量等于分区数量*秒数*maxnum
        SimpleSparkKafka simpleSparkKafka = new SimpleSparkKafka(args);
        simpleSparkKafka.execute(new SKProcessor() {
            @Override
            public void load(JavaStreamingContext jsc, KafkaOffsetPersist kafkaOffsetPersist) {
                //idea中调试时，为了方便，不保存kafka偏移量
                //kafkaOffsetPersist.clearPersist();
            }

            @Override
            public void process(JavaInputDStream<ConsumerRecord<String, String>> directStream) {
                //spark streaming 算子请写在静态方法中，否则会出现序列化错误，可以参考map(directStream)和filter(mapDStream);
                //JavaDStream<DataModel> mapDStream = map(directStream);
                //JavaDStream<DataModel> filter = filter(mapDStream);

                foreachRDDSink(directStream);
                //打印消费的kafka偏移量
                //KafkaTools.printKafkaOffset(directStream);

            }
        }).start();
    }

    public static void foreachRDDSink(JavaInputDStream<ConsumerRecord<String, String>> directStream)
    {
        directStream.foreachRDD(new VoidFunction<JavaRDD<ConsumerRecord<String, String>>>() {
            @Override
            public void call(JavaRDD<ConsumerRecord<String, String>> javaRDD) throws Exception {
                javaRDD.foreachPartition(new VoidFunction<Iterator<ConsumerRecord<String, String>>>() {
                    @Override
                    public void call(Iterator<ConsumerRecord<String, String>> iterator) throws Exception {
                        while (iterator.hasNext())
                        {
                            ConsumerRecord<String, String> next = iterator.next();
                            System.out.println(next.value());
                        }
                    }
                });
            }
        });
    }

    public static JavaDStream<DataModel> filter(JavaDStream<DataModel> javaDStream)
    {
        JavaDStream<DataModel> filter = javaDStream.filter(new Function<DataModel, Boolean>() {
            @Override
            public Boolean call(DataModel v1) throws Exception {
                return v1 != null;
            }
        });
        return filter;
    }

    public static JavaDStream<DataModel> map(JavaInputDStream<ConsumerRecord<String, String>> directStream)
    {
        JavaDStream<DataModel> map = directStream.map(new Function<ConsumerRecord<String, String>, DataModel>() {
            @Override
            public DataModel call(ConsumerRecord<String, String> v1) throws Exception {
                String line = v1.value();
                DataModel dataModel = new DataModel();
                try {
                    JSONObject jsonObject = JSON.parseObject(line);
                    String table = jsonObject.getString(JsonUtil.TABLE);
                    //表名
                    if (StringUtils.isEmpty(table)) {//没有表名
                        return null;
                    } else {
                        dataModel.setTable(table);
                    }

                    //主键
                    JSONArray array = jsonObject.getJSONArray(JsonUtil.PRIMARY_KEYS);
                    if (array == null || array.size() < 1) {//没有主键
                        return null;
                    } else {
                        List<String> keys = JSONArray.parseArray(array.toJSONString(), String.class);
                        List<Tuple3<String,String,String>> KeysList = new ArrayList<>();
                        for (int i = 0; i < keys.size(); i++) {
                            String key = keys.get(i);
                            Tuple3<String,String,String> tuple3 = new Tuple3<>(key,"","");
                            KeysList.add(tuple3);
                        }
                        dataModel.setPrimary_Keys(KeysList);
                    }

                    //操作类型
                    String op_type = jsonObject.getString(JsonUtil.OP_TYPE);
                    if (StringUtils.isEmpty(op_type)) {//没有操作类型
                        return null;
                    } else {
                        dataModel.setOp_Type(op_type);
                    }
                    JSONObject fieldsJSOIN = null;
                    //字段
                    if(op_type.equals(JsonUtil.INSERT))
                    {
                        fieldsJSOIN = (JSONObject) jsonObject.get(JsonUtil.AFTER);
                    }
                    else if(op_type.equals(JsonUtil.UPDATE))
                    {
                        fieldsJSOIN = (JSONObject) jsonObject.get(JsonUtil.AFTER);
                    }
                    else if(op_type.equals(JsonUtil.DELETE))
                    {
                        fieldsJSOIN = (JSONObject) jsonObject.get(JsonUtil.BEFORE);
                    }
                    else
                    {
                        return null;
                    }

                    //字段类型推断
                    List<Tuple3<String,String,String>> fieldsList = new ArrayList<>();
                    List<Tuple3<String, String, String>> primary_keys_new = new ArrayList<>();
                    for(Map.Entry<String,Object> entry : fieldsJSOIN.entrySet() )
                    {
                        String key = entry.getKey();
                        Object value = entry.getValue();
                        if(value==null)
                        {
                            continue;
                        }
                        String type = "";
                        String typeName = value.getClass().getTypeName();
                        Tuple3<String,String,String> tuple3=null;
                        type = DataModel.covertType(typeName);

                        boolean isKey = false;
                        List<Tuple3<String, String, String>> primary_keys = dataModel.getPrimary_Keys();
                        for (int i = 0; i < primary_keys.size(); i++) {
                            Tuple3<String, String, String> keyStr = primary_keys.get(i);
                            if(keyStr._1().equals(key))
                            {//如果是主键，那么设置类型，和值，并且不会放入到fields
                                primary_keys_new.add(new Tuple3<>(key,value.toString(),type));
                                isKey=true;
                                continue;
                            }
                        }
                        if(isKey)
                        {//如果是主键，则不放入fields;
                            continue;
                        }

                        tuple3 = new Tuple3<>(key,value.toString(),type);
                        fieldsList.add(tuple3);
                    }
                    dataModel.setPrimary_Keys(primary_keys_new);
                    dataModel.setFields(fieldsList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dataModel.setJson(line);
                //构建sql语句
                dataModel.createSql();
                return dataModel;
            }
        });
        return map;
    }
}
